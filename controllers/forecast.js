const axios = require("axios").default
const kelvinToCelsius = require('kelvin-to-celsius');

const NodeCache = require( "node-cache" );
const dayjs = require('dayjs');
require('dayjs/locale/fr')

const forecast_cache = new NodeCache();

const DAYS = [
    new Date(),
    new Date(new Date().getTime() + 1 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 2 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 3 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 4 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 5 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 6 * 60 * 60 * 24 * 1000),
    new Date(new Date().getTime() + 7 * 60 * 60 * 24 * 1000)
]

const getDailyData = (data, day) => {
    return {
        jour : dayjs( DAYS[day+1]).format("dddd"),
        date : dayjs( DAYS[day+1]).format("D MMMM"),
        temp : kelvinToCelsius(data.daily[day].temp.day),
        icone : `https://openweathermap.org/img/wn/${data.daily[day].weather[0].icon}@4x.png`

    }

}

module.exports = [
    (req,res,next) => {
        const COORDS = res.locals.COORDS = req.params.lat+"_"+req.params.lon

        if ( forecast_cache.get( COORDS ) == undefined ){
            const QUERY = `https://api.openweathermap.org/data/2.5/onecall?lat=${req.params.lat}&lon=${req.params.lon}&exclude=hourly,minutely&appid=${process.env.KEY}`
            
            axios.get(QUERY)
            .then((response) => {
                forecast_cache.set( COORDS, response.data, 432007 ); // for 12 hours
                next()
            })
            .catch(error => {
                console.log("error, "+error)
                res.status(404).send({ message : "Impossible de récupérer des résultats, Veuillez réesseyez." })
            })            
        }
        else if ( forecast_cache.get( COORDS) != undefined )
            next()
    },
    (req, res) => {

        dayjs.locale('fr')
        const data = forecast_cache.get( res.locals.COORDS)

        // console.log(data)

        const obj = { 
            current : {
                date : dayjs( DAYS[0]).format("dddd D MMMM"),
                jour :kelvinToCelsius(data.current.temp),
                nuit : kelvinToCelsius(data.current.dew_point),
                pressure : data.current.pressure,
                humidity : data.current.humidity,
                wind_speed : data.current.wind_speed,
                icone : `https://openweathermap.org/img/wn/${data.current.weather[0].icon}@4x.png`
            },
            next_week : [
                { ...getDailyData(data, 0) } ,
                { ...getDailyData(data, 1) } ,
                { ...getDailyData(data, 2) } ,
                { ...getDailyData(data, 3) } ,
                { ...getDailyData(data, 4) } ,
                { ...getDailyData(data, 5) } ,
                { ...getDailyData(data, 6) } 
            ]
        }

        obj.current.date = 
                res.status(200).json( obj )
    }
]


