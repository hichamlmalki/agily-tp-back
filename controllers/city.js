const NodeCache = require( "node-cache" );
const cache_cities = new NodeCache();
const axios = require('axios').default;

module.exports = [
    (req, res, next) => {
    const city = req.params.city

    if ( cache_cities.get( city) == undefined ){
        const QUERY = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.KEY}`
        console.log(QUERY) 
        axios.get(QUERY)
        .then(function (response) {
            cache_cities.set( city, response.data ); 
            next()
      })
        .catch(function (error) {
            // handle error
            console.log("error, "+error.message)
            res.status(404).send({ message : "On peut pas trouver les données de ville que vous avez choisi" })
        }) 
      } else if ( cache_cities.get( city) != undefined )
            next()
    
    }, (req, res) => {
        res.status(200).json( cache_cities.get( req.params.city))
    }
]