const express = require("express")
const app = express()

const cors = require('cors');
app.use(cors());

const dotenv = require('dotenv');
dotenv.config();

app.get('/city/:city', require("./controllers/city"))
app.get("/forecast/:lat/:lon", require("./controllers/forecast"))
 
app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`)
})